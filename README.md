# Proyectos Tópicos de Astronomía y Astrofísica I-2022

Repositorio para el acceso de los proyectos generados en el curso de Tópicos de Astrofísica y Astronomía I-2022

Escuela de Física
Tecnológico de Costa Rica

## Authors and acknowledgment
Estudiantado del curso de tópicos I-2022

## License
For open source projects, say how it is licensed.

## Project status
Finalizado, aunque se aceptan _merge requests_
